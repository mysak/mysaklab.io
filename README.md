# MysakFe

This project was developed as a bachelor thesis on Faculty of Information Technology, Czech Technical University in Prague.
Full text of the thesis (in Czech) is available at [https://dspace.cvut.cz/handle/10467/83192](https://dspace.cvut.cz/handle/10467/83192).

Preview of the front-end is deployed at [mysak.gitlab.io](https://mysak.gitlab.io).

## Downloading and installing dependencies
After you clone this repository and change directory into it, it is necessary to download all of the dependencis using `npm` package manager.
If you don't have it on your operating system, you can simply download it using your system's package manager.

### Downloading NPM
E.g. for Ubuntu, run `sudo apt install npm`

### Install dependencies
After you've installed `npm`, all you have to do to start developing is install the dependencies.

Run `npm install` to install all the dependencies.

## Devepolment
Now that you're all set up, you can start developing. Here are some useful commands:  

Note: All of the following commands must be preceeded by `npm run`, e.g. `ng serve` -> `npm run ng serve`.  
If you don't want to write `npm run` before each command, simply install `@angular/cli` globally.  

To do that, run `npm install -g @angular/cli`. (You may have to provide root access depending on the location of your global packages).

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Run `npm run e2e-headless` to execute the end-to-end tests in command line only.  

Note. For the end-to-end tests, `Chromium` browser is required. Please install it using your package manager.
### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
