import { Component, OnInit } from '@angular/core';
import { EduappService } from '../eduapp.service';
import { Observable } from 'rxjs';
import { Eduapp } from '../eduapp.model';

/**
 * Page containing list of all eduapps
 */
@Component({
  selector: 'app-eduapps-page',
  templateUrl: './eduapps-page.component.html',
  styleUrls: ['./eduapps-page.component.scss']
})
export class EduappsPageComponent implements OnInit {

  eduapps$: Observable<Eduapp[]>;

  /**
   * @param eduappService: used to fetch eduapps from the API
   */
  constructor(private eduappService: EduappService) {
  }

  /**
   * Fetch eduapps from the API
   */
  ngOnInit() {
    this.eduapps$ = this.eduappService.getEduapps();
  }

}
