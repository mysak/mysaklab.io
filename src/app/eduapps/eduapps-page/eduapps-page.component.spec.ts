import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduappsPageComponent } from './eduapps-page.component';

describe('EduappsPageComponent', () => {
  let component: EduappsPageComponent;
  let fixture: ComponentFixture<EduappsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduappsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduappsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
