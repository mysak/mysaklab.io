import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EduappsPageComponent } from './eduapps-page/eduapps-page.component';
import { EduappsRoutingModule } from './eduapps-routing.module';
import { EduappDetailPageComponent } from './eduapp-detail-page/eduapp-detail-page.component';
import { ToolbarModule } from '../shared/widgets/toolbar/toolbar.module';
import { MatCardModule, MatGridListModule, MatListModule } from '@angular/material';

@NgModule({
  declarations: [EduappsPageComponent, EduappDetailPageComponent],
  imports: [
    MatCardModule,
    ToolbarModule,
    CommonModule,
    EduappsRoutingModule,
    MatGridListModule,
    MatListModule,
  ]
})
export class EduAppsModule { }
