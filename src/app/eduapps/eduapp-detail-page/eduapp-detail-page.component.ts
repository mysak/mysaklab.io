import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EduappService } from '../eduapp.service';
import { tap } from 'rxjs/operators';

/**
 * Transparent component, whose purpose is
 * to recognize selected eduapp and save it into the EduappService
 */
@Component({
  selector: 'app-eduapp-detail-page',
  templateUrl: './eduapp-detail-page.component.html',
  styleUrls: ['./eduapp-detail-page.component.scss']
})
export class EduappDetailPageComponent implements OnInit {
  selectedEduappId: number;

  /**
   * @param route: used to get eduapp id from route
   * @param eduappService: used to store eduapp id
   */
  constructor(private route: ActivatedRoute,
              private eduappService: EduappService) {
  }

  /**
   * Get eduapp id from active route and store it into EduappService
   */
  ngOnInit() {
    this.route.paramMap.pipe(
      tap(pMap => this.eduappService.setSelectedId(+pMap.get('eid')))
    ).subscribe();
  }
}
