import { Image } from '../shared/models/image.model';

/**
 * Model of an Eduapp fetched from API
 */
export class Eduapp {
  eduappId: number;
  name: string;
  image: Image;
}
