import { Child } from '../children/child.model';
import { Image } from '../shared/models/image.model';

/**
 * Model of a Group fetched from API
 */
export class Group {
  groupId: number;
  name?: string;
  image?: Image;
  children?: Child[];
}
