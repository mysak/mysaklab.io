import { TestBed } from '@angular/core/testing';

import { GroupService } from './group.service';
import { ApiService } from '../shared/services/api.service';
import { TEST_GROUPS } from '../shared/testing/data/Groups';
import { of } from 'rxjs';
import { observeResonse, testError, testHttpGet } from '../shared/testing/utility/service-utilities';
import { GroupsOptions } from '../shared/models/collection-options';

describe('GroupService', () => {
  let groupService: GroupService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  const group$ = of(TEST_GROUPS[0]);
  const groups$ = of(TEST_GROUPS);
  const group = TEST_GROUPS[0];
  const groups = TEST_GROUPS;
  const options: GroupsOptions = {
    limit: 100,
    offset: 50,
    search: 'abc',
    sort: 'name',
  };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet']);
    TestBed.configureTestingModule(
      {
        providers: [
          GroupService,
          {provide: ApiService, useValue: spy},
        ]
      });
    groupService = TestBed.get(GroupService);
    apiServiceSpy = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(groupService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
  });

  describe('#getGroup', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(group$));

    it('should call httpGet with id of group', () =>
      testHttpGet(() => groupService.getGroup(42), apiServiceSpy.httpGet, '/groups/42'));

    it('should return observable of group', () =>
      groupService.getGroup(42)
        .subscribe(res => expect(res).toEqual(group), fail));

    it('should return error on http error', () =>
      testError(() => groupService.getGroup(42), apiServiceSpy.httpGet));
  });

  describe('#getGroups', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(groups)));

    it('should call httpGet with url of /groups', () =>
      testHttpGet(() => groupService.getGroups(options),
        apiServiceSpy.httpGet,
        '/groups',
        options,
      ));

    it('should return observable of groups', () => {
      groupService.getGroups().subscribe(res =>
        expect(res).toEqual(groups), fail);
    });

    it('should return error on http error', () =>
      testError(() => groupService.getGroups(), apiServiceSpy.httpGet));
  });

  describe('#getGroupsWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(groups)));

    it('should call httpGet with url of /groups', () =>
      testHttpGet(() => groupService.getGroupsWithCount(options),
        apiServiceSpy.httpGet,
        '/groups',
        options,
        true));

    it('should return observable of groups and count', () =>
      groupService.getGroupsWithCount().subscribe(res =>
        expect(res).toEqual({data: groups, count: groups.length}), fail));

    it('should return error on http error', () =>
      testError(() => groupService.getGroupsWithCount(), apiServiceSpy.httpGet));
  });
});

