import { ChipItem } from '../shared/widgets/item-chips/chip-item.model';
import { Group } from './group.model';
import { SelectItem } from '../shared/widgets/item-select/select-item';

/**
 * Data class for material chip containing group
 * Data class for material select option containing group
 */
export class GroupItem implements ChipItem, SelectItem {
  image: string;
  id: number;
  label: string;
  constructor(g: Group) {
    this.image = g.image ? g.image.resourceUri : undefined;
    this.label = g.name;
    this.id = g.groupId;
  }
}
