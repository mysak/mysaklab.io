import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Group } from './group.model';
import { processCountHeader, toParams } from '../shared/services/service-utilities';
import { ApiService } from '../shared/services/api.service';
import { map } from 'rxjs/operators';
import { GroupsOptions } from '../shared/models/collection-options';

/**
 * Fetch groups from API using ApiService
 */
@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private groupsUrlSuffix = '/groups';

  /**
   * Requires ApiService to fetch from API
   * @param apiService: uses this service to fetch from API
   */
  constructor(private apiService: ApiService) {
  }

  /**
   * Get groups matching selected options, if no options provided, get all groups
   * Fetches /groups?{options}
   * @param options: allows to specify which groups to retrieve, options are serialized into query params
   * @returns observable of fetched groups matching criteria
   */
  getGroups(options?: GroupsOptions): Observable<Group[]> {
    return this.getGroupsWithCount(options).pipe(map(response => response.data));
  }

  /**
   * Get groups matching selected options, if no options provided, get all groups
   * Fetches /groups?{options}
   * @param options: allows to specify which groups to retrieve, options are serialized into query params
   * @returns observable of fetched groups matching criteria with total count of groups matching criteria
   * (with no respect to pagination)
   */
  getGroupsWithCount(options?: GroupsOptions): Observable<{ data: Group[], count: number }> {
    return processCountHeader(this.apiService.httpGet<Group[]>(
      this.groupsUrlSuffix,
      {params: toParams(options), observe: 'response'}
    ));
  }

  /**
   * Get group with specific ID
   * Fetches /groups/{id} from API
   * @param id: group to fetch
   * @returns observable of fetched group matching id
   */
  getGroup(id: number): Observable<Group> {
    return this.apiService.httpGet<Group>(this.groupsUrlSuffix + '/' + id);
  }
}
