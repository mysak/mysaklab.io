import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { SharedMaterialModule } from '../../../shared/shared-material/shared-material.module';
import { ItemSelectModule } from '../../../shared/widgets/item-select/item-select.module';
import { DataFilteringModule } from '../../../shared/widgets/data-filtering/data-filtering.module';
import { MatExpansionModule, MatListModule } from '@angular/material';
import { ItemChipsModule } from '../../../shared/widgets/item-chips/item-chips.module';

@NgModule({
  declarations: [TasksListComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    ItemSelectModule,
    DataFilteringModule,
    MatExpansionModule,
    MatListModule,
    ItemChipsModule
  ],
  exports: [
    TasksListComponent
  ]
})
export class TasksListModule {}
