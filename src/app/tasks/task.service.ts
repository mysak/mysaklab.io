import { Injectable } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Observable } from 'rxjs';
import { processCountHeader, toParams } from '../shared/services/service-utilities';
import { TaskOptions, TaskSettingsOptions } from '../shared/models/collection-options';
import { Task } from './task.model';
import { map } from 'rxjs/operators';
import { TaskSetting } from './tasksetting.model';


/**
 * Fetch tasks from API using ApiService
 */
@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private tasksUrlSuffix = '/tasks';

  /**
   * Requires ApiService to fetch from API
   * @param apiService: uses this service to fetch from API
   */
  constructor(private apiService: ApiService) {
  }

  /**
   * Get task with specific ID
   * Fetches /tasks/{id} from API
   * @param id: task to fetch
   * @returns observable of fetched task matching id
   */
  getTask(id: number): Observable<Task> {
    return this.apiService.httpGet<Task>(
      this.tasksUrlSuffix + '/' + id,
    );
  }

  /**
   * Get tasks matching selected options, if no options provided, get all tasks
   * Fetches /tasks?{options}
   * @param options: allows to specify which tasks to retrieve, options are serialized into query params
   * @returns observable of fetched tasks matching criteria
   */
  getTasks(options?: TaskOptions): Observable<Task[]> {
    return this.getTasksWithCount(options).pipe(map(response => response.data));
  }

  /**
   * Get tasks matching selected options, if no options provided, get all tasks
   * Fetches /tasks?{options}
   * @param options: allows to specify which tasks to retrieve, options are serialized into query params
   * @returns observable of fetched tasks matching criteria with total count of tasks matching criteria
   * (with no respect to pagination)
   */
  getTasksWithCount(options?: TaskOptions): Observable<{ data: Task[], count: number }> {
    return processCountHeader(this.apiService.httpGet<Task[]>(
      this.tasksUrlSuffix,
      {params: toParams(options), observe: 'response'}
    ));
  }

  /**
   * Get taskSettings of given task matching selected options, if no options provided, get all taskSettings
   * Fetches /tasks/{id}/tasksettings?{options}
   * @param taskId: fetch taskSettings of this task
   * @param options: allows to specify which taskSettings to retrieve, options are serialized into query params
   * @returns observable of fetched taskSettings matching criteria
   */
  getTaskSettings(taskId: number, options?: TaskSettingsOptions): Observable<TaskSetting[]> {
    return this.getTaskSettingsWithCount(taskId, options).pipe(map(res => res.data));
  }

  /**
   * Get taskSettings of given task matching selected options, if no options provided, get all taskSettings
   * Fetches /tasks/{id}/tasksettings?{options}
   * @param taskId: fetch taskSettings of this task
   * @param options: allows to specify which taskSettings to retrieve, options are serialized into query params
   * @returns observable of fetched taskSettings matching criteria with total count of taskSettings matching criteria
   * (with no respect to pagination)
   */
  getTaskSettingsWithCount(taskId: number, options?: TaskSettingsOptions): Observable<{ data: TaskSetting[], count: number }> {
    return processCountHeader(this.apiService.httpGet<TaskSetting[]>(
      this.tasksUrlSuffix + '/' + taskId + '/tasksettings',
      {params: toParams(options), observe: 'response'}
    ));
  }
}
