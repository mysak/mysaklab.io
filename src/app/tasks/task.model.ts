import { Image } from '../shared/models/image.model';
import { TaskSetting } from './tasksetting.model';
import { Category } from '../categories/category.model';

/**
 * Model of a Task fetched from API
 */
export class Task {
  taskId: number;
  name: string;
  categories: Category[];
  image: Image;
  taskSettings?: TaskSetting[];
}
