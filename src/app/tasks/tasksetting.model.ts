import { Image } from '../shared/models/image.model';

/**
 * Model of a TaskSetting fetched from API
 */
export class TaskSetting {
  taskSettingId: number;
  taskId: number;
  name: string;
  difficulty: number;
  image: Image;
}
