import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * Routes:
 *   / --> /eduapps
 *   /eduapps --> LazyLoaded EduAppsModule
 */
const routes: Routes = [
  {
    path: 'eduapps',
    loadChildren: './eduapps/eduapps.module#EduAppsModule'
  },
  {
    path: '',
    redirectTo: 'eduapps',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
