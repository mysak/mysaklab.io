import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SelectItem } from './select-item';
import { MatSelectChange } from '@angular/material';

/**
 * Customized material select box
 */
@Component({
  selector: 'app-item-select',
  templateUrl: './item-select.component.html',
  styleUrls: ['./item-select.component.scss']
})
export class ItemSelectComponent implements OnInit {

  @Output() selectionChange = new EventEmitter<MatSelectChange>();
  @Input() items: SelectItem[];
  selectedId: number;
  @Input() placeholder: string;
  // ID of initially selected item
  @Input() initialItemId: number;
  // if the selection box should shrink on smaller screens
  @Input() shrinkResponsive = false;

  constructor() {
  }

  ngOnInit() {
    this.selectedId = this.initialItemId;
  }
}
