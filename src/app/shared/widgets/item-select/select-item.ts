export interface SelectItem {
  image?: string;
  label: string;
  id: any;
}
