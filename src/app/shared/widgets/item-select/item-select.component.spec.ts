/* tslint:disable:component-selector */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSelectComponent } from './item-select.component';
import { Component, DebugElement, EventEmitter, Input, Output } from '@angular/core';
import { TEST_GROUPS } from '../../testing/data/Groups';
import { By } from '@angular/platform-browser';
import { GroupItem } from '../../../groups/group-item';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatInputModule, MatOption, MatSelect, MatSelectModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

let component: TestHostComponent;
let fixture: ComponentFixture<TestHostComponent>;
let page: Page;
const SELECTED_GROUP = 2;

describe('ItemSelectComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestHostComponent,
        ItemSelectComponent
      ],
      imports: [
        SharedMaterialModule,
        MatSelectModule,
        NoopAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(async(() => {
    createComponent();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture).toBeTruthy();
    expect(page).toBeTruthy();
  });

  describe('display options', () => {
    let itemSelectComponent: ItemSelectComponent;
    beforeEach(() => {
      itemSelectComponent = page.itemSelectDe.componentInstance;
    });

    it('should pass correct input', () => {
      // Input items match
      // Input items ids match
      expect(itemSelectComponent.items.map(t => t.id)).toEqual(TEST_GROUPS.map(g => g.groupId));
      // Input items labels match
      expect(itemSelectComponent.items.map(t => t.label)).toEqual(TEST_GROUPS.map(g => g.name));
      // Input items labels match
      expect(itemSelectComponent.items.map(t => t.image)).toEqual(TEST_GROUPS.map(g => g.image.resourceUri));

      // Selected group match
      expect(itemSelectComponent.selectedId).toBe(SELECTED_GROUP);

      // Placeholder match
      expect(itemSelectComponent.placeholder).toBe('Skupiny');
    });

    it('should display items given as input parameter', () => {
      fixture.detectChanges();
      const expected = TEST_GROUPS.map(g => g.name);
      expected.splice(0, 0, '--');
      expect(page.matOptionInstances.map(o => o.getLabel()))
        .toEqual(expected);
    });
  });
});

// ----------------HELPERS-------------------

@Component({
  template: `
    <app-item-select [items]="testItems"
                     (selectionChange)="testOnChange($event)"
                     [placeholder]="placeholder"
                     [initialItemId]="initialItemId">
    </app-item-select>`
})
class TestHostComponent {
  testItems = TEST_GROUPS.map(g => new GroupItem(g));
  testOnChange = jasmine.createSpy('testOnChange');
  initialItemId = SELECTED_GROUP;
  placeholder = 'Skupiny';
}

@Component({
  template: `<p class="test-option">{{text}}</p>`,
  selector: 'mat-option'
})
class MatOptionStubComponent {
  @Input() text: string;
  @Input() value: number;
}

@Component({
  template: `
    <ng-content></ng-content>`,
  selector: 'mat-select'
})
class MatSelectStubComponent {
  @Input() placeholder: string;
  @Input() value: number;
  @Output() selectionChange = new EventEmitter<MatSelect>();
}

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

function createComponent() {
  fixture = TestBed.createComponent(TestHostComponent);
  component = fixture.componentInstance;
  page = new Page(fixture);

  // 1st change detection triggers ngOnInit
  fixture.detectChanges();
}


// https://angular.io/guide/testing#use-a-page-object
class Page {
  private fixture: ComponentFixture<TestHostComponent>;

  get matSelectDe(): DebugElement {
    return this.itemSelectDe.queryAll(By.css('mat-select'))[0];
  }

  get itemSelectDe(): DebugElement {
    return fixture.debugElement.queryAll(By.css('app-item-select'))[0];
  }

  get matOptionInstances(): MatOption[] { return this.matSelectDe.componentInstance.options; }

  constructor(compFixture: ComponentFixture<TestHostComponent>) {
    this.fixture = compFixture;
  }
}
