import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemChipsComponent } from './item-chips.component';
import { MatChipsModule } from '@angular/material';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';

@NgModule({
  declarations: [ItemChipsComponent],
  imports: [
    CommonModule,
    MatChipsModule,
    SharedMaterialModule
  ],
  exports: [
    ItemChipsComponent
  ]
})
export class ItemChipsModule { }
