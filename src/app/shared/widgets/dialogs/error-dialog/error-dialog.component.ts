import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ErrorDialogData } from './error-dialog-data';

/**
 * Displays dialog with a red error message
 */
@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {

  /**
   * @param data: injected on component creation, contains dialog title and content
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ErrorDialogData) {
  }

  ngOnInit() {
  }
}
