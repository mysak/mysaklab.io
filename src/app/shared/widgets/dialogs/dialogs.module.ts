import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { A11yModule } from '@angular/cdk/a11y';
import { ForbiddenStringValidatorDirective } from './forbidden-string-validator.directive';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';

@NgModule({
  declarations: [ConfirmDialogComponent, ForbiddenStringValidatorDirective, InputDialogComponent, ErrorDialogComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    A11yModule,
  ],
  exports: [ ConfirmDialogComponent, InputDialogComponent, ErrorDialogComponent],
  entryComponents: [ ConfirmDialogComponent, InputDialogComponent, ErrorDialogComponent]
})
export class DialogsModule {}
