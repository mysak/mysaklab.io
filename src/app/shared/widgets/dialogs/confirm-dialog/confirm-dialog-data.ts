import { DialogData } from '../dialog-data';

export interface ConfirmDialogData extends DialogData {
  title: string;
  content?: string;
}
