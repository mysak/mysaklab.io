import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

/*
LEGAL NOTICE:
This directive is taken from the official angular documentation
https://angular.io/guide/form-validation#custom-validators
MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

/**
 * check if input field string matches forbidden string
 */
@Directive({
  selector: '[appForbiddenString]',
  providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenStringValidatorDirective, multi: true}]
})
export class ForbiddenStringValidatorDirective implements Validator {
  @Input('appForbiddenString') string: string;

  constructor() {
  }

  /**
   * @param control: get value from input field
   * @returns error state if control value matches string
   */
  validate(control: AbstractControl): { [key: string]: any } | null {
    return this.string && control.value === this.string ? {'forbiddenString': {value: control.value}} :
      null;
  }

}
