import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { QueueItem } from './queue-item.model';

/**
 * Draggable queue of items, every item has an image, label, secondary label and delete button
 * Queue supports handling first item separately
 */
@Component({
  selector: 'app-item-queue',
  templateUrl: './item-queue.component.html',
  styleUrls: ['./item-queue.component.scss']
})
export class ItemQueueComponent implements OnInit {
  // First item in queue, if undefined, first item is handled no differently than others
  @Input() firstItem: QueueItem;
  // Emit if remove button on first item was clicked
  @Output() firstItemRemove = new EventEmitter<QueueItem>();
  // Loading indicator
  @Input() loading = false;
  @Input() items: QueueItem[];
  // Emit if order or number of items changed
  @Output() itemsChange = new EventEmitter<QueueItem[]>();

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * Called when user drops an item,
   * moves the item to correct position in the item array and emits change
   * @param event: information about where user dropped the item
   */
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.items, event.previousIndex, event.currentIndex);
    this.itemsChange.emit(this.items);
  }

  /**
   * Called when user clicks on remove button
   * removes the item from the item array and emits change
   * @param idx: index of "to be removed" item
   */
  removeItem(idx: number) {
    this.items.splice(idx, 1);
    this.itemsChange.emit(this.items);
  }

  /**
   * Called when user clicks on remove button in the first item
   * Removing first item is considered a more significant event, therefore
   * this component only emits event and lets superior components deal with the removal
   */
  removeFirstItem() {
    this.firstItemRemove.emit(this.firstItem);
  }
}
