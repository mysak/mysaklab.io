import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataFilteringComponent } from './data-filtering.component';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material';
import { MatPaginatorIntlCz } from './translate';

@NgModule({
  declarations: [DataFilteringComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    MatPaginatorModule,
  ],
  exports: [
    DataFilteringComponent
  ],
  providers: [
    {provide: MatPaginatorIntl, useClass: MatPaginatorIntlCz}
  ]
})
export class DataFilteringModule {}
