import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PageEvent } from '@angular/material';
import { CollectionOptions } from '../../models/collection-options';

/**
 * This component contains paginator, search bar and takes other filtering components as children
 */
@Component({
  selector: 'app-data-filtering',
  templateUrl: './data-filtering.component.html',
  styleUrls: ['./data-filtering.component.scss']
})
export class DataFilteringComponent implements OnInit {
  @Input() pageSize: number;
  @Input() paginate: boolean;
  @Input() count: number;
  @Output() updateOptions = new EventEmitter<CollectionOptions>();

  ngOnInit() {
  }

  /**
   * When searchString changes, emit new value
   * @param searchString: new search string
   */
  onSearch(searchString: string) {
    this.updateOptions.emit({search: searchString});
  }

  /**
   * When page or page size changes, emit new limit and offset
   * @param page: event containing changes to pagination
   */
  onPageChanged(page: PageEvent) {
    this.pageSize = page.pageSize ? page.pageSize : this.pageSize;
    this.updateOptions.emit({
      limit: this.pageSize,
      offset: page.pageIndex ? page.pageIndex * page.pageSize : 0
    });
  }
}
