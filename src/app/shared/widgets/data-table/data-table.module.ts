import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table.component';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatCheckboxModule, MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  declarations: [DataTableComponent],
  imports: [
    SharedMaterialModule,
    CommonModule,
    MatSortModule,
    MatCheckboxModule,
    MatTableModule
  ],
  exports: [
    DataTableComponent
  ]
})
export class DataTableModule {}
