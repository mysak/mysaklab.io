import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataTableColumn, DataTableCustomColumn } from './data-table-column';
import { Sort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() data: object[];
  @Input() columnDefinitions: DataTableColumn[];
  @Input() additionalColumns: DataTableCustomColumn[];
  @Input() displayedColumns: string[];
  @Input() loading = false;
  // Emit when user clicked on sorting arrow
  @Output() sortChange = new EventEmitter<Sort>();
  @Output() rowClick = new EventEmitter<any>();
  selection = new SelectionModel<any>(true, []);
  // Emit when user selected or deselected an item
  @Output() selectionChange = this.selection.changed;

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * @returns true if all items currently shown on screen are selected
   */
  isAllSelected() {
    return this.data
      .map(e => this.selection.selected.includes(e))
      .reduce((acc, current) => acc && current, true);
  }

  // Function inspired by the official material reference
  // https://material.angular.io/components/table/overview
  masterToggle() {
    this.isAllSelected() ?
      // Deselects all currently shown items
      this.selection.deselect(...this.data) :
      // Selects all currently shown items
      this.selection.select(...this.data);
  }
  onRowClick(row: any) {
    if (this.displayedColumns.includes('select')) {
      this.selection.toggle(row);
    }
    this.rowClick.emit(row);
  }
}
