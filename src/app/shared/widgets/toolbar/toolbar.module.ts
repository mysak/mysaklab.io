import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { ToolbarComponent } from './toolbar.component';
import { RouterModule } from '@angular/router';
import { MatDividerModule } from '@angular/material';
import { ItemSelectModule } from '../item-select/item-select.module';

@NgModule({
  declarations: [ToolbarComponent],
  imports: [
    RouterModule,
    CommonModule,
    SharedMaterialModule,
    MatDividerModule,
    ItemSelectModule,
  ],
  exports: [CommonModule, SharedMaterialModule, ToolbarComponent]
})
export class ToolbarModule {}
