import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EduappService } from '../../../eduapps/eduapp.service';
import { map } from 'rxjs/operators';
import { EduappItem } from '../../../eduapps/eduapp-item';
import { MatSelectChange } from '@angular/material';

/**
 * Navigation bar with app title, navigation buttons and eduapp selection
 */
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  selectedEduappId: number;
  eduapps$: Observable<EduappItem[]>;

  /**
   * @param router: used to navigate to different route when changing eduapp
   * @param eduappService: used to get list of eduapps and selected eduapp
   */
  constructor(private router: Router,
              private eduappService: EduappService) {
  }

  ngOnInit() {
    this.eduapps$ = this.eduappService.getEduapps().pipe(
      map(eduapps => eduapps.map(e => new EduappItem(e)))
    );
    this.selectedEduappId = this.eduappService.getSelectedId();
  }

  /**
   * Change router when different eduapp is selected
   * If user was on specific page (assign, detail), get down to the /scenarios or /children page
   * @param selected: newly selected eduapp
   */
  onEduappSelect(selected: MatSelectChange) {
    const route = this.router.url;
    let redir = '';
    if (route.includes('/children')) {
      // user was on /children/... page
      redir = '/children';
    } else if (route.includes('scenarios')) {
      // user was on /scenarios/... page
      redir = '/scenarios';
    }
    const url = selected.value ? '/eduapps/' + selected.value + redir : '';

    // Workaround to reload components when eduapp ID changes
    // This is easier then injecting eduapp service or activated route into
    // every component to wait for url changes
    // can fail due to guards, should keep selected value if failed
    this.router.navigateByUrl('/').then(succeeded =>
      succeeded ?
        this.router.navigateByUrl(url) :
        Promise.resolve(false)
    ).then(succeeded => {
      if (!succeeded) {
        selected.source.writeValue(this.selectedEduappId);
      }
    });
  }

}
