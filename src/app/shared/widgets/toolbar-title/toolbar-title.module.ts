import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarTitleComponent } from './toolbar-title.component';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ToolbarTitleComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    RouterModule,
  ],
  exports: [
    ToolbarTitleComponent,
  ]
})
export class ToolbarTitleModule {}
