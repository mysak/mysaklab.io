import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCheckboxModule, MatChipList, MatChipsModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatProgressSpinnerModule,
  MatRippleModule,
  MatSnackBarModule,
  MatToolbarModule, MatTooltipModule,

} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

/**
 * Module for common material and layout UI widgets and directive
 */
@NgModule({
  exports: [
    MatRippleModule,
    MatButtonModule,
    MatInputModule,
    MatChipsModule,
    FormsModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatDialogModule,
  ]
})
export class SharedMaterialModule {}
