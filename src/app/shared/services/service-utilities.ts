import { HttpParams, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Transform options given by parameter to HttpParams
 * @param options: transform each key, value pair of this object to HttpParams key, value pair
 * if options have key, but value is undefined, that key is ignored
 * @returns HttpParams containing keys and values of options, empty HttpParams
 * if options are undefined or do not have any defined keys
 */
export const toParams = (options: Object): HttpParams => {
  let params = new HttpParams();
  if (!options) {return params; }
  Object.keys(options).forEach(
    // check if undefined and then set to params
    key => options[key] !== undefined ? params = params.set(key, options[key].toString()) : ''
  );
  return params;
};

/**
 * Factory that takes behavior subject storing options and returns a method with new options as an argument,
 * that updates the behaviour subject to the new value of options, if the new options and current value of
 * subject differ, otherwise emits the current value again
 * @param subject: behavior subject to update (has notion of current value)
 */
export const updateBehaviorSubject = <T>(subject: BehaviorSubject<T>) => (options: T) => {
  const nextOptions = Object.assign({}, subject.getValue());
  // flag if options and current value differ
  let changed = false;
  Object.keys(options).forEach(key => {
    // check each key in options, if changed, set new key to nextOptions
    if (options[key] !== nextOptions[key]) {
      changed = true;
      nextOptions[key] = options[key];
    }
  });
  // emit new options or current value if unchanged
  subject.next(changed ? nextOptions : subject.getValue());
};

/**
 * Get 'X-Total-Count' header from response and return an object with data and count keys
 * @param o: httpResponse to process
 */
export const processCountHeader = <T>(o: Observable<HttpResponse<T>>): Observable<{ data: T, count: number }> => o.pipe(
  map(response => {
    return {
      data: response.body,
      count: response.headers.has('X-Total-Count') ? Number(response.headers.get('X-Total-Count')) : undefined
    };
  }));
