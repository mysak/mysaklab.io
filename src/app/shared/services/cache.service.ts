import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the
MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

// Maximum cache age in milliseconds
const maxAge = environment.MAX_CACHE_AGE_MS;

export interface RequestCacheEntry {
  url: string;
  response: HttpResponse<any>;
  lastRead: number;
}

/**
 * This service serves as a temporary in memory cache store
 * It uses a map to store key, value pairs, where key is the url and value the http response
 */
@Injectable({
  providedIn: 'root'
})
export class CacheService {
  // map as a cache
  cache = new Map<string, RequestCacheEntry>();

  /**
   * Check if requested url is in cache and hasn't expired and if so, retrieve cached response
   * @param req: check for url of this request (with params)
   * @returns cached response or undefined if not found
   */
  get(req: HttpRequest<any>): HttpResponse<any> | undefined {
    const url = req.urlWithParams;
    const cached = this.cache.get(url);

    if (!cached) { return undefined; }

    const isExpired = cached.lastRead < (Date.now() - maxAge);
    return isExpired ? undefined : cached.response;
  }

  /**
   * Puts response to cache under request URL
   * @param req: store the response under this request url with params
   * @param response: cache this under request url
   */
  put(req: HttpRequest<any>, response: HttpResponse<any>): void {
    const url = req.urlWithParams;

    this.cache.set(url, {url, response, lastRead: Date.now()});

    // Remove expired cache entries
    const expired = Date.now() - maxAge;
    this.cache.forEach(entry => {
      if (entry.lastRead < expired) {
        this.cache.delete(entry.url);
      }
    });
  }
}
