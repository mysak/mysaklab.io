/**
 * Sorting and filtering options common for all collections
 */
export interface CollectionOptions {
  limit?: number;
  offset?: number;
  search?: string;
  sort?: string;
}

/**
 * Below are options specific to each collections
 */

export interface ChildrenOptions extends CollectionOptions {
  groupId?: number;
  gender?: string;
}

export interface ScenariosOptions extends CollectionOptions {
}

export interface TaskOptions extends CollectionOptions {
  categoryId?: number;
}

export interface CategoryOptions extends CollectionOptions {
}

export interface TaskSettingsOptions extends CollectionOptions {
}

export interface GroupsOptions extends CollectionOptions {
}
