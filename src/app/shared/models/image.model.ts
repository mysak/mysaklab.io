/**
 * Model of image dimensions fetched from the API
 */
class Dimensions {
  width: number;
  height: number;
}

/**
 * Model of Image fetched from the API
 */
export class Image {
  resourceUri: string;
  mimeType: string;
  fileSize: number;
  createdOn: string;
  dimensions: Dimensions;
}
