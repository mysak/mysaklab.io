import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from '../../../children/child.model';

/**
 * Transform Gender into string representing gender
 */
@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  /**
   * Transform Gender into string
   * @param value: Gender to transform
   * @param args: optional args, not used
   * @returns string representation of gender
   */
  transform(value: Gender, args?: any): string {
    return value === Gender.female ? 'žena' : 'muž';
  }
}
