import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLinkDirectiveStub } from './utility/component-utilities';
import { ItemSelectStubComponent } from './stubs/item-select.stub.component';

/**
 * This module has no purpose, other than suppress typescript
 * "Not included in a module" error
 */
@NgModule({
  declarations: [RouterLinkDirectiveStub, ItemSelectStubComponent],
  imports: [
    CommonModule
  ]
})
export class FakeTestingModule { }
