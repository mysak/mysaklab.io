import { TEST_EDUAPPS } from '../data/Eduapps';
import { asyncData } from '../utility/component-utilities';

export class EduappServiceSpy {
  selected: number;

  getEduapps = jasmine.createSpy('getEduapps').and.callFake(
    () => asyncData(Object.assign([], TEST_EDUAPPS)));

  getSelectedId = jasmine.createSpy('getSelectedId').and.callFake(
    () => this.selected);

  setSelectedId = jasmine.createSpy('setSelectedId').and.callFake(
    (id: number) => this.selected = id);
}
