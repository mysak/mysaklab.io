import { HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { CollectionOptions } from '../../models/collection-options';
import { HttpTestingController } from '@angular/common/http/testing';
import { MatDialog, MatSnackBar } from '@angular/material';
import { tick } from '@angular/core/testing';

export function testError<T>(testFn: () => Observable<T>,
                             spy: jasmine.Spy,
                             err?: HttpErrorResponse): void {
  const error = err ? err : new HttpErrorResponse({error: '404 test error', status: 404});
  spy.and.returnValue(throwError(error));
  testFn().subscribe(
    () => fail('expected an error'),
    e => expect(e.error).toBe(error.error));
}

const toParams = (options: CollectionOptions): HttpParams => {
  let params = new HttpParams();
  Object.keys(options).forEach(key => params = params.set(key, options[key].toString()));
  return params;
};

export function testHttpGet<T>(testFn: () => Observable<T>,
                               spy: jasmine.Spy,
                               url: string,
                               options?: CollectionOptions,
                               mustObserve?: boolean): void {
  testHttp(testFn, spy, url, undefined, options, mustObserve);
}

export function testHttpPut<T>(testFn: () => Observable<T>,
                               spy: jasmine.Spy,
                               url: string,
                               data: object | object[],
                               options?: CollectionOptions,
                               mustObserve?: boolean): void {
  testHttp(testFn, spy, url, data, options, mustObserve);
}

export function testHttpPost<T>(testFn: () => Observable<T>,
                                spy: jasmine.Spy,
                                url: string,
                                data: object | object[],
                                options?: CollectionOptions,
                                mustObserve?: boolean): void {
  testHttp(testFn, spy, url, data, options, mustObserve);
}

export function testHttpPatch<T>(testFn: () => Observable<T>,
                                 spy: jasmine.Spy,
                                 url: string,
                                 data: object | object[],
                                 options?: CollectionOptions,
                                 mustObserve?: boolean): void {
  testHttp(testFn, spy, url, data, options, mustObserve);
}

export function testHttpDelete<T>(testFn: () => Observable<T>,
                                  spy: jasmine.Spy,
                                  url: string,
                                  options?: CollectionOptions,
                                  mustObserve?: boolean): void {
  testHttp(testFn, spy, url, undefined, options, mustObserve);
}


function testHttp<T>(testFn: () => Observable<T>, spyMethod: jasmine.Spy,
                     url: string, data: object | object[], options?: CollectionOptions, mustObserve?: boolean) {
  testFn().subscribe(res => res, fail);
  expect(spyMethod).toHaveBeenCalledTimes(1);
  const args = spyMethod.calls.mostRecent().args;
  const httpOptions = data ? args[2] : args[1];
  const httpData = data ? args[1] : undefined;

  expect(args[0]).toBe(url);

  if (!httpOptions && (options || mustObserve)) {
    fail();
    return;
  }

  data ?
    expect(httpData).toEqual(data) :
    expect(httpData).toBeUndefined();

  options ?
    expect(httpOptions.params).toEqual(toParams(options)) :
    expect(httpOptions ? httpOptions.hasOwnProperty('params') : httpOptions).toBeFalsy();

  mustObserve ?
    expect(httpOptions.observe).toBe('response') :
    expect(['response', undefined]).toContain(httpOptions ? httpOptions.observe : httpOptions);
}

export enum SpyType {
  DIALOG,
  SNACKBAR
}

// Must be in fakeAsync environment
export function testHandleError<T>(fn: (message: string) => Observable<T>,
                                   httpController: HttpTestingController,
                                   spy: jasmine.SpyObj<MatSnackBar | MatDialog>,
                                   spyType: SpyType,
                                   params: { method: string, url: string }) {
  const message = 'test user error';
  fn(message).subscribe(() => fail('Should trow error'),
    (err: HttpErrorResponse) => {
      expect(err.status).toBe(404);
      expect(err.error).toBe('test error');
    });

  const req = httpController.expectOne(params);
  req.flush('test error', {status: 404, statusText: 'Not Found'});

  // Wait for snackbar or dialog to open
  tick();
  // Should open snackbar or dialog
  expect(spy.open).toHaveBeenCalledTimes(1);
  spyType === SpyType.DIALOG ?
    expect(spy.open.calls.mostRecent().args[1].data.content).toBe(message) :
    expect(spy.open.calls.mostRecent().args[0]).toBe(message);
}


export const observeResonse = <T>(data: T[]): Observable<HttpResponse<T[]>> =>
  of(new HttpResponse<T[]>({
    body: data,
    headers: new HttpHeaders({'X-Total-Count': data.length.toString()})
  }));
