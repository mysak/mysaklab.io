import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectItem } from '../../widgets/item-select/select-item';
import { MatSelect, MatSelectChange } from '@angular/material';

@Component({selector: 'app-item-select', template: ''})
export class ItemSelectStubComponent {
  @Output() selectionChange = new EventEmitter<MatSelectChange>();

  @Input() items: SelectItem[];
  selectedId: number;

  matSelectSpy: jasmine.SpyObj<MatSelect> = jasmine.createSpyObj('MatSelect', ['writeValue']);

  @Input() placeholder: string;
  // ID of initially selected item
  @Input() initialItemId: number;
  // if the selection box should shrink on smaller screens
  @Input() shrinkResponsive = false;

  _selectId(id: number) {
    this.selectionChange.emit(new MatSelectChange(this.matSelectSpy, id));
  }
}

