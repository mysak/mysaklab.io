import { Scenario } from '../../../scenarios/scenario.model';
import { TEST_TASKS } from './Tasks';
import { TEST_TASKSETTINGS } from './TaskSettings';

export const TEST_SCENARIOS: Scenario[] = [
  {
    scenarioId: 1,
    name: 'Myšlení 1',
    lastChange: '2018-01-05T08:40:51.620Z',
    note: '',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149090.svg',
      mimeType: 'image/jpeg',
      fileSize: 12985,
      createdOn: '2018-01-05T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }, items: [
      {
        type: 'task',
        data: TEST_TASKS.find(t => t.taskId === 1)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 1)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 2)
      }
    ]
  }, {
    scenarioId: 2,
    name: 'Zrak - těžší',
    lastChange: '2018-01-06T08:40:51.620Z',
    note: '',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149090.svg',
      mimeType: 'image/jpeg',
      fileSize: 12483,
      createdOn: '2018-01-05T10:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    items: [
      {
        type: 'task',
        data: TEST_TASKS.find(t => t.taskId === 2)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 3)
      }
    ]
  }, {
    scenarioId: 3,
    name: 'Zrak - lehčí aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    lastChange: '2018-01-06T08:40:51.620Z',
    note: 'Liší se v třídění aaaaaaaaaaaaaaaaaaaaaaaaaaa',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149090.svg',
      mimeType: 'image/jpeg',
      fileSize: 43384,
      createdOn: '2017-09-01T12:00:00Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    items: [
      {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 4)
      }, {
        type: 'task',
        data: TEST_TASKS.find(t => t.taskId === 2)
      }
    ]
  }, {
    scenarioId: 4,
    name: 'Paměť',
    lastChange: '2018-01-01T08:40:51.620Z',
    note: '',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149090.svg',
      mimeType: 'image/jpeg',
      fileSize: 52985,
      createdOn: '2017-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    items: [
      {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 4)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 3)
      }
    ]
  }, {
    scenarioId: 5,
    name: 'Všeobecný test',
    lastChange: '2018-01-01T08:45:50.220Z',
    note: '',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149090.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    items: [
      {
        type: 'task',
        data: TEST_TASKS.find(t => t.taskId === 1)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 1)
      }, {
        type: 'taskSetting',
        data: TEST_TASKSETTINGS.find(t => t.taskSettingId === 2)
      }
    ]
  }
];
