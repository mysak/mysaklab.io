/**
 * Compares two values of the same type which must be one of:
 * string, number
 */
export const compareValues = (a, b, orderInverted) => {
  const aType = typeof a;
  const bType = typeof b;
  if (aType !== bType) { return null; }
  switch (aType) {
    case 'string':
      return orderInverted ?
        b.localeCompare(a) :
        a.localeCompare(b);
    case 'number':
      return orderInverted ? b - a : a - b;
    default:
      console.error('NON COMPARABLE: ' + aType, a, b);
  }
};

/**
 * Sorts list of object by its keys respectively
 * @param list: objects to sort
 * @param keys: keys of objects, ordered by most important first
 */
export const sortByKeys = (list, keys) => {
  list.sort((a, b) => {
    for (let i = 0; i < keys.length; i++) {
      let orderInverted = false;
      let key = keys[i].toString();
      if (key[0] === '-') {
        key = key.substring(1);
        orderInverted = true;
      }
      const res = compareValues(a[key], b[key], orderInverted);
      if (res !== 0) {
        return res;
      }
    }
    return 0;
  });
};

