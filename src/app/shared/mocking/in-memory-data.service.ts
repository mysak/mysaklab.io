import { Injectable } from '@angular/core';
import { getStatusText, InMemoryDbService, RequestInfo, ResponseOptions, STATUS } from 'angular-in-memory-web-api';

import { of } from 'rxjs';
import { Child } from '../../children/child.model';
import { sortByKeys } from './Helpers';
import { Group } from '../../groups/group.model';
import { Scenario, ScenarioSaveItem, ScenarioUpdate } from '../../scenarios/scenario.model';
import { Eduapp } from '../../eduapps/eduapp.model';
import { Task } from '../../tasks/task.model';
import { environment } from '../../../environments/environment';
import { createImage, generateData, SCENARIO_IMAGE } from './generate-data';

// Introduce bugs and errors to test for error messages
const DEBUG = false;

/**
 * Get the name of an id field based on the collection name
 * @param collectionName: collection to return id of
 */
const getIdName = (collectionName: string): string => {
  switch (collectionName) {
    case 'children':
      return 'childId';
    case 'groups':
      return 'groupId';
    case 'scenarios':
      return 'scenarioId';
    case 'tasks':
      return 'scenarioId';
    default:
      return undefined;
  }
};

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  // All of the "database tables"
  COLLECTIONS: {
    children: Child[],
    groups: Group[],
    scenarios: Scenario[],
    eduapps: Eduapp[],
    tasks: Task[]
  };


  // Generate test data and store them in COLLECTIONS variable
  createDb() {
    this.COLLECTIONS = generateData();
    const db = this.COLLECTIONS;
    return of(db);
  }

  // PUT request handler
  put(reqInfo: RequestInfo) {
    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP PUT override');
      console.log(reqInfo);
      const collection = reqInfo.collection;
      const id = reqInfo.id;
      let data;
      if (id) {
        const idx = collection.findIndex(item => item[getIdName(reqInfo.collectionName)] === +id);

        // Update collections
        switch (reqInfo.collectionName) {
          case 'scenarios':
            collection[idx] = this.mapBodyToScenario(
              // @ts-ignore
              reqInfo.req.body as ScenarioUpdate,
              +id,
              collection[idx].image.resourceUri);
            break;
          case 'children':
            // @ts-ignore
            const scenarioQueueIds: { scenarioId: number }[] = reqInfo.req.body;
            console.log('SCENARIOS: ' + scenarioQueueIds);
            (collection[idx] as Child).scenarioQueue.scenarios = scenarioQueueIds.map(sq =>
              this.COLLECTIONS.scenarios.find(s => s.scenarioId === sq.scenarioId)
            );
        }
        // @ts-ignore
        data = collection[idx];
      }

      const options: ResponseOptions = {
        status: DEBUG && Number(reqInfo.id) === 2 ? STATUS.NOT_FOUND : STATUS.OK,
        body: data,
      };
      return this.finishOptions(options, reqInfo);
    });
  }

  // PATCH request handler
  patch(reqInfo: RequestInfo) {
    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP PATCH override');
      console.log(reqInfo);
      // Get list from comma separated children ids
      const childrenIds = reqInfo.id.split(',').map(id => +id);
      const assignTo = this.COLLECTIONS.children.filter(ch => childrenIds.includes(ch.childId));
      // @ts-ignore
      const scIdx = this.COLLECTIONS.scenarios.findIndex(sc => sc.scenarioId === reqInfo.req.body.scenarioId);
      const scenario: Scenario = this.COLLECTIONS.scenarios[scIdx];

      assignTo.forEach(ch => {
          if (reqInfo.url.includes('back')) {
            ch.scenarioQueue.scenarios.push(scenario);
          } else {
            ch.scenarioQueue.front.inProgress ?
              ch.scenarioQueue.scenarios.splice(1, 0, scenario) :
              ch.scenarioQueue.scenarios.splice(0, 0, scenario);
          }
        }
      );
      const options: ResponseOptions = {
        status: DEBUG && Number(reqInfo.id) === 1 ? STATUS.NOT_FOUND : STATUS.OK
      };
      return this.finishOptions(options, reqInfo);
    });
  }

  delete(reqInfo: RequestInfo) {
    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP DELETE override');
      console.log(reqInfo);

      if (reqInfo.collectionName === 'scenarios') {
        const idx = this.COLLECTIONS.scenarios.findIndex(s => s.scenarioId === +reqInfo.id);
        this.COLLECTIONS.scenarios.splice(idx, 1);
      }

      if (reqInfo.collectionName === 'children') {
        const idx = this.COLLECTIONS.children.findIndex(ch => ch.childId === +reqInfo.id);
        this.COLLECTIONS.children[idx].scenarioQueue.front.inProgress = false;
        this.COLLECTIONS.children[idx].scenarioQueue.scenarios.splice(0, 1);
      }

      const options: ResponseOptions = {
        status: DEBUG && +reqInfo.id === 1 ? STATUS.NOT_FOUND : STATUS.OK
      };
      return this.finishOptions(options, reqInfo);
    });
  }

  // POST request handler
  post(reqInfo: RequestInfo) {
    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP POST override');
      console.log(reqInfo);
      const collection: Child[] | Group[] | Scenario[] | Eduapp[] | Task[] = Object.assign([], reqInfo.collection);
      const id = reqInfo.id;
      // @ts-ignore
      const data = id === undefined ? collection[0] : collection.find(item => item[getIdName(reqInfo.collectionName)] === Number(id));
      // @ts-ignore
      const status = DEBUG && reqInfo.req.body && (reqInfo.req.body.name) === 'Myšák' ? STATUS.NOT_FOUND : STATUS.OK;
      const options: ResponseOptions = {
        status,
        body: data,
      };
      // @ts-ignore
      if (reqInfo.collectionName === 'scenarios' && reqInfo.req.body) {
        const scenarios = reqInfo.collection as Scenario[];
        const scenarioId = scenarios[scenarios.length - 1].scenarioId + 1;
        // @ts-ignore
        const scenarioUpdate = reqInfo.req.body as ScenarioUpdate;
        const scenario = this.mapBodyToScenario(scenarioUpdate, scenarioId, SCENARIO_IMAGE);
        // Update scenarios collection
        reqInfo.collection.push(scenario);
        options.body = scenario;
      }
      return this.finishOptions(options, reqInfo);
    });
  }

  // GET request handler
  get(reqInfo: RequestInfo) {
    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP GET override');
      console.log(reqInfo);

      let collection: Child[] | Group[] | Scenario[] | Eduapp[] | Task[] = Object.assign([], reqInfo.collection);

      // Make first scenario always edited by someone else
      if (DEBUG) { this.COLLECTIONS.scenarios[0].lastChange = Date(); }

      // Filter collection
      collection = this.filterCollection(reqInfo, collection);
      // Sort collection
      if (reqInfo.query.has('sort')) { sortByKeys(collection, [reqInfo.query.get('sort')]); }

      const id = reqInfo.id;
      // @ts-ignore
      let data = id === undefined ? collection : collection.find(item => item[getIdName(reqInfo.collectionName)] === +id);
      if (id && reqInfo.collectionName === 'tasks' && reqInfo.req.url.includes('tasksettings')) {
        data = this.COLLECTIONS.tasks.find(t => t.taskId === +id).taskSettings;
      }

      if (reqInfo.collection === undefined && reqInfo.url === environment.apiBase + '/eduapps') {
        data = this.COLLECTIONS.eduapps;
      }

      const options: ResponseOptions = data ?
        {body: data, status: STATUS.OK} : {status: STATUS.NOT_FOUND};
      reqInfo.headers = reqInfo.headers.set('X-Total-Count', collection.length.toString());
      return this.finishOptions(options, reqInfo);
    });
  }

  // -------------------------HELPERS----------------------------

  /**
   * Filter collection (for GET request)
   * @param reqInfo: GET request info
   * @param collection: input collection before filtering
   * @return filtered collection
   */
  filterCollection(reqInfo, collection: Child[] | Group[] | Scenario[] | Eduapp[] | Task[]):
    Child[] | Group[] | Scenario[] | Eduapp[] | Task[] {
    const query = reqInfo.query;
    let res = collection;
    // Search filter
    if (query.has('search')) {
      switch (reqInfo.collectionName) {
        case 'children':
          res = (res as Child[]).filter(ch =>
            ch.firstName.toLowerCase().includes(query.get('search')[0].toLowerCase()) ||
            ch.lastName.toLowerCase().includes(query.get('search')[0].toLowerCase())
          );
          break;
        case 'groups':
          res = (res as Group[]).filter(g => g.name.includes(query.get('search')[0]));
          break;
        case 'scenarios':
          res = (res as Scenario[]).filter(s =>
            s.name.toLowerCase().includes(query.get('search')[0].toLowerCase()));
          break;
        case 'tasks':
          res = (res as Task[]).filter(s =>
            s.name.toLowerCase().includes(query.get('search')[0].toLowerCase()) ||
            s.taskSettings.find(t => t.name.toLowerCase().includes(query.get('search')[0].toLowerCase()))
          );
          break;
      }
    }

    if (query.has('groupId')) { // Filter by group
      res = (res as Child[]).filter(
        ch => ch.groups.map(g => g.groupId).includes(Number(query.get('groupId')[0]))
      );
    } else if (query.has('categoryId')) { // Filter by category
      res = (res as Task[]).filter(
        t => t.categories.map(c => c.categoryId).includes(Number(query.get('categoryId')[0]))
      );
    }

    if (query.has('gender')) { // Filter by gender
      res = (res as Child[]).filter(ch => ch.gender === query.get('gender')[0]);
    }

    if (query.has('limit')) { // Pagination (limit / offset)
      const start = query.has('offset') ? Number(query.get('offset')) : 0;
      const end = Number(query.get('limit')) + start;
      res = res.slice(start, end);
    }
    return res;
  }

  /**
   * Map scenario update items to appropriate tasks or tasksettings
   * @param update: API payload with ids of tasks and/or tasksettings
   * @param id: scenario identifier
   * @param image: new scenario image
   */
  mapBodyToScenario(update: ScenarioUpdate, id: number, image: string): Scenario {
    return {
      name: update.name,
      note: update.note,
      scenarioId: id,
      lastChange: Date(),
      image: createImage(image),
      items: update.tasks.map((task: ScenarioSaveItem) => {
        const reqTask = this.COLLECTIONS.tasks.find(t => t.taskId === task.taskId);
        return {
          type: task.type,
          data: task.type === 'task' ?
            reqTask :
            reqTask.taskSettings.find(t => t.taskSettingId === task.taskSettingId)
        };
      })
    };
  }

  /**
   * Finalize HTTP Response options
   * HELPER from https://github.com/angular/in-memory-web-api/blob/master/src/app/hero-in-mem-data-override.service.ts
   */
  finishOptions(options: ResponseOptions, {headers, url}: RequestInfo) {
    options.statusText = getStatusText(options.status);
    options.headers = headers;
    options.url = url;
    return options;
  }
}

