import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildDetailPageComponent } from './child-detail-page.component';

describe('ChildDetailPageComponent', () => {
  let component: ChildDetailPageComponent;
  let fixture: ComponentFixture<ChildDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChildDetailPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
