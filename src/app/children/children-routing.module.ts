import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChildrenPageComponent } from './children-page/children-page.component';
import { ChildDetailPageComponent } from './child-detail-page/child-detail-page.component';
import { UnsavedChangesCanDeactivateGuard } from '../shared/guards/unsaved-changes-can-deactivate.guard';

/**
 * Routes:
 *   / --> ChildrenPage
 *   /{id} --> ChildDetailPage
 */
const routes: Routes = [
  {
    path: '',
    component: ChildrenPageComponent
  },
  {
    path: ':id',
    component: ChildDetailPageComponent,
    canDeactivate: [UnsavedChangesCanDeactivateGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChildrenRoutingModule {}
