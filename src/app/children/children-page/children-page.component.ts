import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

/**
 * Page that contains children list
 */
@Component({
  selector: 'app-children-page',
  templateUrl: './children-page.component.html',
  styleUrls: ['./children-page.component.scss']
})
export class ChildrenPageComponent implements OnInit {
  paginate = environment.PAGINATE;

  constructor() { }

  ngOnInit() {
  }

}
