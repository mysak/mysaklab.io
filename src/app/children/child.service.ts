import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Child, ScenarioQueue } from './child.model';
import { processCountHeader, toParams } from '../shared/services/service-utilities';
import { ApiService } from '../shared/services/api.service';
import { map } from 'rxjs/operators';
import { ChildrenOptions } from '../shared/models/collection-options';
import { Scenario } from '../scenarios/scenario.model';
import { ScenarioQueueItem } from '../scenarios/scenario-queue-item.model';

export enum AssignmentType {
  FRONT = 'front',
  BACK = 'back'
}

/**
 * Fetch and alter children from API using ApiService
 */
@Injectable({
  providedIn: 'root'
})
export class ChildService {

  private childrenUrlSuffix = '/children';

  /**
   * Requires ApiService to fetch from API
   * @param apiService: uses this service to fetch from API
   */
  constructor(private apiService: ApiService) {
  }

  /**
   * Get child with specific ID
   * Fetches /children/{id} from API
   * @param id: child to fetch
   * @returns observable of fetched child matching id
   */
  getChild(id: number): Observable<Child> {
    return this.apiService.httpGet<Child>(
      this.childrenUrlSuffix + '/' + id,
    );
  }

  /**
   * Get children matching selected options, if no options provided, get all children
   * Fetches /children?{options}
   * @param options: allows to specify which children to retrieve, options are serialized into query params
   * @returns fetched children matching criteria
   */
  getChildren(options?: ChildrenOptions): Observable<Child[]> {
    return this.getChildrenWithCount(options).pipe(map(response => response.data));
  }

  /**
   * Get children matching selected options, if no options provided, get all children
   * Fetches /children?{options}
   * @param options: allows to specify which children to retrieve, options are serialized into query params
   * @returns fetched categories matching criteria with total count of categories matching criteria
   * (with no respect to pagination)
   */
  getChildrenWithCount(options?: ChildrenOptions): Observable<{ data: Child[], count: number }> {
    return processCountHeader(this.apiService.httpGet<Child[]>(
      this.childrenUrlSuffix,
      {params: toParams(options), observe: 'response'}
    ));
  }

  /**
   * Updates scenario queue of child given by parameter
   * Sends PUT request to /children/{id}/scenario-queue
   * @param id: child to update
   * @param queueItems: items to set to child queue
   * @returns observable of updated scenario queue
   */
  updateScenarioQueue(id: number, queueItems: ScenarioQueueItem[]): Observable<ScenarioQueue> {
    return this.apiService.httpPut<ScenarioQueue>(
      this.childrenUrlSuffix + '/' + id + '/scenario-queue',
      queueItems.map(q => {
        return {scenarioId: q.id};
      }));
  }

  /**
   * Assigns new scenario to front / back of the queue of children given by parameter
   * Sends PATCH request to /children/{ids separated by commas}/scenario-queue/{back or front}
   * @param childrenIds: children to assign new scenarios to
   * @param scenarioId: id of scenario to assign
   * @param type: FRONT or BACK of the queue
   * @returns observable of assigned scenario
   */
  assignScenario(childrenIds: number[], scenarioId: number, type: AssignmentType): Observable<Scenario> {
    return this.apiService.httpPatch<Scenario>(
      this.childrenUrlSuffix + '/' + childrenIds.join() + '/scenario-queue/' + type,
      {scenarioId}
    );
  }

  /**
   * Cancels first scenario of child given by parameter
   * Sends DELETE request to /children/{id}/scenario-queue/front
   * If there is no scenario assigned, it silently succeeds
   * @param childId: child to whom cancel the first scenario
   */
  cancelFirstScenario(childId: number) {
    return this.apiService.httpDelete(
      this.childrenUrlSuffix + '/' + childId + '/scenario-queue/front'
    );
  }
}
