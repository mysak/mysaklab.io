import { Component, Input, OnInit } from '@angular/core';
import { Child } from '../../../child.model';
import { GroupItem } from '../../../../groups/group-item';

/**
 * Non editable detail of child, displays child Image, Name, Age, Gender and Groups
 */
@Component({
  selector: 'app-child-detail',
  templateUrl: './child-detail.component.html',
  styleUrls: ['./child-detail.component.scss']
})
export class ChildDetailComponent implements OnInit {

  @Input() child: Child;
  @Input() groupChips: GroupItem[];

  constructor() {
  }

  ngOnInit() {
  }
}
