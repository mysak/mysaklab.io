import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChildDetailComponent } from './child-detail/child-detail.component';
import { SharedMaterialModule } from '../../../shared/shared-material/shared-material.module';
import { ItemChipsModule } from '../../../shared/widgets/item-chips/item-chips.module';
import { UtilityModule } from '../../../shared/utility/utility.module';

@NgModule({
  declarations: [ChildDetailComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    ItemChipsModule,
    UtilityModule
  ],
  exports: [
    ChildDetailComponent
  ]
})
export class ChildDetailModule { }
