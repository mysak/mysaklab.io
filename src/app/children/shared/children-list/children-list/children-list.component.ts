import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Child, Gender } from '../../../child.model';
import { ChildService } from '../../../child.service';
import { Sort } from '@angular/material';
import { BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { GroupService } from '../../../../groups/group.service';
import { updateBehaviorSubject } from '../../../../shared/services/service-utilities';
import { ChildrenOptions } from '../../../../shared/models/collection-options';
import { SelectionChange } from '@angular/cdk/collections';
import { EduappService } from '../../../../eduapps/eduapp.service';
import { GroupItem } from '../../../../groups/group-item';
import { SelectItem } from '../../../../shared/widgets/item-select/select-item';
import { Router } from '@angular/router';

/**
 * Sortable, searchable list of children, can be also filtered by group
 * Fetches children data from ChildService
 */
@Component({
  selector: 'app-children-list',
  templateUrl: './children-list.component.html',
  styleUrls: ['./children-list.component.scss']
})
export class ChildrenListComponent implements OnInit, OnDestroy {
  eduappId: number;

  children: Child[] = [];
  genderItems: SelectItem[] = [{id: Gender.male, label: 'Muž'}, {id: Gender.female, label: 'Žena'}];
  // Columns that are displayed
  @Input() displayedColumns: String[];
  // Buttons that are displayed, allowed values: 'assign', 'delete', 'edit'
  @Input() actionButtons: ('add' | 'edit') [];
  // Whether the list should be paginated
  @Input() paginate: boolean;
  private groupsSubscription: Subscription;
  private childrenSubscription: Subscription;
  childrenCount: number;
  pageSize: number;

  // state of sorting, pagination, etc...
  options: BehaviorSubject<ChildrenOptions>;
  updateOptions: (options: ChildrenOptions) => void;

  loading = false;
  groupsItems: GroupItem[] = [];

  // Emit if user selected or unselected a checkbox
  @Output() selectionChange = new EventEmitter<SelectionChange<Child>>();
  @Input() listTitle: string;

  /**
   * @param childService: used to get list of children
   * @param groupService: used to get list of groups
   * @param router: used to navigate to child detail
   * @param eduappService: used to get selected eduapp ID for edit button redirect
   */
  constructor(
    private childService: ChildService,
    private groupService: GroupService,
    private router: Router,
    private eduappService: EduappService,
  ) {
  }

  /**
   * On init fetch as many children as default page size, fetch all groups
   */
  ngOnInit() {
    // Get eduapp ID from route
    this.eduappId = this.eduappService.getSelectedId();
    this.pageSize = environment.DEFAULT_PAGE_SIZE;
    // Create subject options to store filtering state
    const defaultOptions = this.paginate ? {limit: this.pageSize, offset: 0} : {};
    this.options = new BehaviorSubject<ChildrenOptions>(defaultOptions);
    this.updateOptions = updateBehaviorSubject(this.options);

    // Subscribe to options to refetch every time options change
    this.childrenSubscription = this.options.pipe(
      distinctUntilChanged(),
      tap(() => this.loading = true),
      debounceTime(environment.DEBOUNCE),
      switchMap(options => this.childService.getChildrenWithCount(options)),
      tap(response => this.childrenCount = response.count),
      map(response => response.data)
    ).subscribe(
      children => {
        this.children = children;
        this.loading = false;
      }
    );
    // Fetch all groups
    this.groupsSubscription = this.groupService.getGroups()
      .subscribe(groups => {
          // Map groups to group chip/select items
          this.groupsItems = groups.map(g => new GroupItem(g));
        }
      );
  }

  /**
   * Called when user selects group, this updates options
   * @param groupId: selected group, if undefined, options are updated with no group selected
   */
  selectGroup(groupId: number): void {
    this.updateOptions({groupId: groupId});
  }

  selectGender(gender: Gender) {
    this.updateOptions({gender});
  }

  /**
   * Filter groups by ids
   * @param groupIds: match these ids
   * @returns only those chip items matching ids in parameter
   */
  filterGroups(groupIds: { groupId: number }[]): GroupItem[] {
    return this.groupsItems.filter(g =>
      groupIds.map(gid => gid.groupId).includes(g.id));
  }

  /**
   * Called when user click on sort by column
   * @param sort: options containing sorted column and direction
   * Updates options with new sort query params
   */
  onSort(sort: Sort): void {
    // Age column id 'age' does not match API name 'dateOfBirth', therefore the following translation
    const sortField = sort.active === 'age' ? 'dateOfBirth' : sort.active;
    this.updateOptions({
      sort: (sort.direction === 'desc' ? '-' : '') + sortField
    });
  }

  /**
   * Unsubscribe from active subscriptions
   */
  ngOnDestroy() {
    this.groupsSubscription.unsubscribe();
    this.childrenSubscription.unsubscribe();
  }

  onChildClick(ch: Child) {
    if (!this.displayedColumns.includes('select')) {
      this.router.navigateByUrl('/eduapps/' + this.eduappId + '/children/' + ch.childId);
    }
  }
}
