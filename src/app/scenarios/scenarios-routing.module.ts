import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScenariosPageComponent } from './scenarios-page/scenarios-page.component';
import { ScenarioDetailPageComponent } from './scenario-detail-page/scenario-detail-page.component';
import { ScenarioAssignPageComponent } from './scenario-assign-page/scenario-assign-page.component';
import { UnsavedChangesCanDeactivateGuard } from '../shared/guards/unsaved-changes-can-deactivate.guard';

/**
 * Routes:
 *   / --> ScenariosPage
 *   /{id} --> ScenarioDetailPage
 *   /{id}/assign --> ScenarioAssignPage
 */
const routes: Routes = [
  {
    path: '',
    component: ScenariosPageComponent
  },
  {
    path: ':id',
    component: ScenarioDetailPageComponent,
    canDeactivate: [UnsavedChangesCanDeactivateGuard]
  },
  {
    path: ':id/assign',
    component: ScenarioAssignPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScenariosRoutingModule {}
