import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

/**
 * Page that contains children list
 */
@Component({
  selector: 'app-scenarios-page',
  templateUrl: './scenarios-page.component.html',
  styleUrls: ['./scenarios-page.component.scss']
})
export class ScenariosPageComponent implements OnInit {
  paginate = environment.PAGINATE;

  constructor() { }

  ngOnInit() {
  }

}
