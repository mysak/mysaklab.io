import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioAssignPageComponent } from './scenario-assign-page.component';

describe('ScenarioAssignPageComponent', () => {
  let component: ScenarioAssignPageComponent;
  let fixture: ComponentFixture<ScenarioAssignPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScenarioAssignPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioAssignPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
