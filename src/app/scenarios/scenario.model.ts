import { Image } from '../shared/models/image.model';
import { Task } from '../tasks/task.model';
import { TaskSetting } from '../tasks/tasksetting.model';

// Maximum scenario name input length
export const MAX_NAME_LEN = 40;
// Maximum scenario note input length
export const MAX_NOTE_LEN = 80;

/**
 * Model of a Scenario fetched from API
 */
export class Scenario {
  scenarioId: number;
  name: string;
  lastChange: string;
  note: string;
  image: Image;
  items?: { type: 'task' | 'taskSetting', data: Task | TaskSetting }[];
}

/**
 * Model of scenario items which are sent to API
 */
export interface ScenarioSaveItem {
  taskId: number;
  taskSettingId?: number;
  type: 'task' | 'taskSetting';
}

/**
 * Model of creating or updating scenario data which are sent to API
 */
export class ScenarioUpdate {
  constructor(name: string, note: string, tasks: ScenarioSaveItem[]) {
    this.name = name;
    this.note = note;
    this.tasks = tasks;
  }

  name: string;
  note: string;
  tasks: ScenarioSaveItem[];
}
