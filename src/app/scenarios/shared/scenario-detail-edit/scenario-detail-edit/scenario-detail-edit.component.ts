import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MAX_NAME_LEN, MAX_NOTE_LEN, Scenario } from '../../../scenario.model';

/**
 * Editable detail of scenario, displays scenario Image, Name and Note
 */
@Component({
  selector: 'app-scenario-detail-edit',
  templateUrl: './scenario-detail-edit.component.html',
  styleUrls: ['./scenario-detail-edit.component.scss']
})
export class ScenarioDetailEditComponent implements OnInit, OnChanges {
  @Input() scenario: Scenario;
  @Input() edit = false;
  @Input() small = false;
  // emit if name or note changes
  @Output() change = new EventEmitter<{ name: string, note: string }>();
  name = '';
  note = '';
  MAX_NAME_LEN = MAX_NAME_LEN;
  MAX_NOTE_LEN = MAX_NOTE_LEN;

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * Called when input properties change, used to set name and note to new values
   * @param changes: changed input properties
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('scenario') && changes.scenario.currentValue) {
      const scenario: Scenario = changes.scenario.currentValue;
      this.name = scenario.name;
      this.note = scenario.note;
    }
  }

  /**
   * Called when name changes, emits change event
   * @param name: new name
   */
  onNameChange(name: string): void {
    this.change.emit({name, note: this.note});
  }

  /**
   * Called when note changes, emits change event
   * @param note: new note
   */
  onNoteChange(note: string): void {
    this.change.emit({name: this.name, note});
  }
}
