import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioDetailEditComponent } from './scenario-detail-edit.component';

describe('ScenarioDetailEditComponent', () => {
  let component: ScenarioDetailEditComponent;
  let fixture: ComponentFixture<ScenarioDetailEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScenarioDetailEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioDetailEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
