import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScenariosListComponent } from './scenarios-list/scenarios-list.component';
import { SharedMaterialModule } from '../../../shared/shared-material/shared-material.module';
import { MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';
import { DataFilteringModule } from '../../../shared/widgets/data-filtering/data-filtering.module';
import { RouterModule } from '@angular/router';
import { DataTableModule } from '../../../shared/widgets/data-table/data-table.module';

@NgModule({
  declarations: [ScenariosListComponent],
  imports: [
    MatSortModule,
    MatTableModule,
    CommonModule,
    SharedMaterialModule,
    MatPaginatorModule,
    DataFilteringModule,
    RouterModule,
    DataTableModule
  ],
  exports: [ScenariosListComponent]
})
export class ScenariosListModule { }
