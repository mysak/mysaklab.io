// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBase: 'http://private-1df806-mysak.apiary-mock.com',
  DEBOUNCE: 10,
  SNACKBAR_DURATION: 2000,
  MAX_CACHE_AGE_MS: 30000,
  DEFAULT_PAGE_SIZE: 10,
  PAGINATE: false,
  // NEVER CACHE INDIVIDUAL CHILD or SCENARIO WITH CACHE-THEN-REFETCH STRATEGY,
  // may cause user to loose his queue changes
  // By default only GET requests are cacheable, use string or regex
  CACHEABLE: ['/eduapps', '/children', '/scenarios', '/tasks', '/groups', '/categories',
    /\/tasks\/.*\/tasksettings/
  ],
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
