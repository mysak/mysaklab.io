#!/bin/bash

app_name="$1"
process_name="$2"

image_id=$(docker inspect "registry.heroku.com/$app_name/$process_name" --format={{.Id}})
payload='{"updates":[{"type":"'$process_name'","docker_image":"'"$image_id"'"}]}'
curl -n -X PATCH "https://api.heroku.com/apps/$app_name/formation" \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_AUTH_TOKEN"
