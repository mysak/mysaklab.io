import { ChildrenList } from './children.list';
import { browser, by, element } from 'protractor';

export enum AssignmentType {
  FRONT,
  BACK,
}

export class ScenariosAssignPage {
  childrenList: ChildrenList;
  constructor() {this.childrenList = new ChildrenList(); }

  getChildByName = (name) => this.childrenList.getChildByName(name);

  getSelectedEduappTitle() { return element(by.css('app-toolbar .mat-select')).getText(); }

  navigateTo(id: number) { return browser.get('/eduapps/1/scenarios/' + id + '/assign'); }

  assignTo(childNames: string[], type: AssignmentType) {
    childNames.forEach(name => element(by.cssContainingText('tr', name)).click());
    this.clickAssign(type);
  }

  assignToGroup(groupName: string, type: AssignmentType) {
    this.selectGroup(groupName);
    this.selectAll();
    this.clickAssign(type);
  }

  clickAssign(type: AssignmentType) {
    type === AssignmentType.BACK ?
      this.getAssignToBackButton().click() :
      this.getAssignToFrontButton().click();
  }

  selectAll() { element(by.css('th mat-checkbox')).click(); }

  selectGroup(name) {
    element(by.cssContainingText('mat-select', 'Skupiny')).click();
    element(by.cssContainingText('mat-option', name)).click();
  }

  getAssignToFrontButton() { return element(by.cssContainingText('button', 'začátek')); }

  getAssignToBackButton() { return element(by.cssContainingText('button', 'konec')); }
}
