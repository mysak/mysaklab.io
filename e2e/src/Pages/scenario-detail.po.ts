import { browser, by, element, ExpectedConditions, Key } from 'protractor';

export class ScenarioDetailPage {
  navigateTo(id?: number) { return browser.get('/eduapps/1/scenarios/' + (id ? id : 'new')); }

  getNameInput() { return element(by.css('#name-input')); }

  getNoteInput() { return element(by.css('#note-input')); }

  getSubmitButton() { return element(by.css('#save')); }

  getTaskSettingByName(name: string) { return element(by.cssContainingText('mat-list-item', name)); }

  getTaskByName(name: string) { return element(by.cssContainingText('mat-expansion-panel', name)); }

  getQueueContent() { return element.all(by.css('.queue-card')); }

  getQueueTitles() { return this.getQueueContent().map(e => e.element(by.css('.mat-body')).getText()); }

  getQueueCard(name: string) { return element(by.cssContainingText('.queue-card', name)); }

  confirmDialog() { element(by.cssContainingText('button', 'Ok')).click(); }

  saveAs(saveAs: string) {
    element(by.css('#save-as')).click();
    const input = element(by.css('#input-dialog-input'));
    input.clear();
    browser.wait(ExpectedConditions.textToBePresentInElementValue(input, ''), 1000);
    element(by.css('#input-dialog-input')).sendKeys(saveAs);
    this.confirmDialog();
  }
}
