import { by, element } from 'protractor';

export class ToolbarNavigation {
  goToChildren() { element(by.cssContainingText('nav a', 'Děti')).click(); }
  goToScenarios() { element(by.cssContainingText('nav a', 'Scénáře')).click(); }
}
