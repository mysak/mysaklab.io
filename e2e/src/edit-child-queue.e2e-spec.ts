import { browser, by, ExpectedConditions } from 'protractor';
import { dragAndDrop, matchUrl } from './helpers';
import { ToolbarNavigation } from './Pages/toolbar.po';
import { ChildDetailPage } from './Pages/child-detail.po';
import { ChildrenPage } from './Pages/children.po';

/**
 * TESTING FOLLOWING USE CASE:
 * UC5: Edit child queue
 * For detailed information on use cases please see documentation
 */
describe('child scenario queue edit', () => {
  let childDetailPage: ChildDetailPage;
  let childrenPage: ChildrenPage;
  let toolbarNavigation: ToolbarNavigation;

  beforeEach(() => {
    childDetailPage = new ChildDetailPage();
    childrenPage = new ChildrenPage();
    toolbarNavigation = new ToolbarNavigation();
    childrenPage.navigateTo();
  });

  /**
   * Verify children queue content
   * @start Children page
   * @end Children page
   * @param name: child to verify
   * @param expectedContent: expected queue items
   */
  const verifyQueue = (name: string, expectedContent: string[]) => {
    childrenPage.getChildByName(name).click();
    // Should navigate to Child detail page
    matchUrl('eduapps\/1\/children\/[0-9]*');

    expect(childDetailPage.getQueueTitles()).toEqual(expectedContent,
      'queue content should match');

    toolbarNavigation.goToChildren();
  };

  /**
   * Edits child queue content, add one scenario, delete one scenario and swap two scenarios
   * Expecting child with name given by parameter to have the following queue content:
   * +------------------------------------+
   * | Motorika | Řeč - těžší | Řeč lehčí |
   * +------------------------------------+
   * @param name: child with expected queue content
   */
  const editQueue = (name: string) => {
    childrenPage.getChildByName(name).click();

    // Add scenario 'Motorika 2'
    childDetailPage.getScenarioByName('Motorika 2').element(by.css('button')).click();

    // Remove scenario 'Řeč - těžší'
    childDetailPage.getQueueCard('Řeč - těžší').element(by.css('button')).click();

    // Swap 'Řeč - lehčí' and 'Motorika 1'
    const elem = childDetailPage.getQueueCard('Řeč - lehčí').element(by.css('.queue-item-drag-handle'));
    const target = childDetailPage.getQueueCard('Motorika 1');
    dragAndDrop(elem, target);

    childDetailPage.getSubmitButton().click();
    childDetailPage.confirmDialog();
    // Workaround - wait until url location changes
    const sep = browser.baseUrl.endsWith('/') ? '' : '/';
    browser.wait(ExpectedConditions.urlIs(browser.baseUrl + sep + 'eduapps/1/children'), 1000);
    return ['Řeč - lehčí', 'Motorika 1', 'Motorika 2'];
  };

  it('should edit scenario queue correctly', () => {
    const newQueueContent = editQueue('Pospíchal');

    // Should return to children page
    matchUrl('eduapps/1/children');

    verifyQueue('Pospíchal', newQueueContent);
  });

  // Save As - Works only sometimes, unstable
  // it('should edit scenario and save as', () => {
  //   const newName = 'NEW NAME ' + Date.now();
  //   const newQueueContent = editScenario(scenarioName, newName);
  //   matchUrl('eduapps/1/scenarios');
  //
  //   // Verify old scenario;
  //   console.log(scenarioName);
  //   verifyScenario(scenarioName, scenarioName + ' NOTE', queueContent);
  //   // Verify new scenario
  //   verifyScenario(newName, scenarioName + ' NOTE EDIT', newQueueContent);
  // });


});
