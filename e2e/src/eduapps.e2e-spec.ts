import { EduappsPage } from './Pages/eduapps.po';
import { ChildrenPage } from './Pages/children.po';
import { matchUrl } from './helpers';

/**
 * TESTING EDUAPP SELECTION
 */
describe('eduapps page', () => {
  let eduappsPage: EduappsPage;
  let childrenPage: ChildrenPage;

  beforeEach(() => {
    eduappsPage = new EduappsPage();
    childrenPage = new ChildrenPage();
    eduappsPage.navigateTo();
  });

  it('should navigate on click and show selected eduapp in toolbar', () => {
    expect(eduappsPage.getTitleText()).toEqual('Vyberte výukovou aplikaci:');
    eduappsPage.getEduappByName('Myšák').click();
    matchUrl('eduapps/1/children');
    expect(childrenPage.getSelectedEduappTitle()).toEqual('Myšák');
  });
});
