import { ScenariosPage } from './Pages/scenarios.po';
import { browser, by } from 'protractor';
import { AssignmentType, ScenariosAssignPage } from './Pages/scenarios-assign.po';
import { ChildrenPage } from './Pages/children.po';
import { ToolbarNavigation } from './Pages/toolbar.po';
import { ChildDetailPage } from './Pages/child-detail.po';
import { matchUrl } from './helpers';

/**
 * TESTING FOLLOWING USE CASES:
 * UC3: Assign scenario to children
 * UC4: Assign scenario to group of children
 * For detailed information on use cases please see documentation
 */
describe('assign scenario', () => {
  let scenariosPage: ScenariosPage;
  let scenariosAssignPage: ScenariosAssignPage;
  let toolbarNavigation: ToolbarNavigation;
  let childrenPage: ChildrenPage;
  let childDetailPage: ChildDetailPage;

  beforeEach(() => {
    scenariosPage = new ScenariosPage();
    scenariosAssignPage = new ScenariosAssignPage();
    childrenPage = new ChildrenPage();
    childDetailPage = new ChildDetailPage();
    toolbarNavigation = new ToolbarNavigation();
  });

  /**
   * Assign scenario to one or more children (or whole group of children)
   * @start scenarios page
   * @end scenarios page
   * @param name: scenario to assign
   * @param type: assign to FRONT / BACK
   * @param childrenNames: children to assign to
   * @param groupName: group to assign to
   */
  const assignScenario = (name: string, type: AssignmentType, childrenNames?: string[], groupName?: string) => {
    scenariosPage.getScenarioByName(name).element(by.css('button.assign')).click();

    // Should navigate to Scenario Assign Page
    matchUrl('eduapps\/1\/scenarios\/[0-9]*\/assign');

    // Assign to group or individual children
    groupName ?
      scenariosAssignPage.assignToGroup(groupName, type) :
      scenariosAssignPage.assignTo(childrenNames, type);
    toolbarNavigation.goToScenarios();
  };

  /**
   * Verify queues of all children specified in previousQueues object to have new scenario assigned
   * @start any (children/scenarios)
   * @end children
   * @param scenarioName: scenario that has been assigned
   * @param previousQueues: previous queue content of each child
   * @param type: assignment to BACK / FRONT of queue
   */
  const verifyChildrenQueues = (scenarioName: string,
                                previousQueues: { [s: string]: { queue: string[], front: boolean } },
                                type: AssignmentType) => {
    Object.keys(previousQueues).forEach(childName => {
      // Navigate to children page without refreshing
      toolbarNavigation.goToChildren();
      // Should navigate to Children page
      matchUrl('eduapps/1/children');

      childrenPage.getChildByName(childName).click();

      // Should navigate to Child detail page
      matchUrl('eduapps\/1\/children\/[0-9]*');

      const expectedContent = previousQueues[childName].queue.slice(0);
      // If first scenario is in progress
      const front = previousQueues[childName].front;
      type === AssignmentType.BACK ?
        expectedContent.push(scenarioName) :
        // if first in progress, assigned scenario should be 2nd
        expectedContent.splice(front ? 1 : 0, 0, scenarioName);

      expect(childDetailPage.getQueueTitles()).toEqual(expectedContent);
    });
    toolbarNavigation.goToChildren();
  };

  /**
   * Go through all children given by name and find their current queue content
   * @start any (children/scenarios)
   * @end Children Page
   * @param childrenNames: children to scan
   */
  const getChildrenQueues = async (childrenNames: string[]):
    Promise<{ [s: string]: { queue: string[], front: boolean } }> => {
    const res = {};
    for (let i = 0; i < childrenNames.length; i++) {
      const name = childrenNames[i];
      toolbarNavigation.goToChildren();
      childrenPage.getChildByName(name).click();
      res[name] = {
        queue: await childDetailPage.getQueueTitles(),
        front: await childDetailPage.getInProgress()
      };
    }
    toolbarNavigation.goToChildren();
    return res;
  };


  it('should assign "Motorika 2" scenario correctly to one child', async () => {
    childrenPage.navigateTo();
    const previousQueueContent = await getChildrenQueues(['Komárková']);

    toolbarNavigation.goToScenarios();
    assignScenario('Motorika 2', AssignmentType.BACK, ['Komárková']);

    verifyChildrenQueues('Motorika 2', previousQueueContent, AssignmentType.BACK);
  });

  it('should assign "Motorika 2" correctly to group "Medvídci"', async () => {
    childrenPage.navigateTo();
    // Check first 5 children
    const childrenNames = (await childrenPage.getChildrenLastNames('Medvídci')).slice(0, 5);
    const previousQueueContent = await getChildrenQueues(childrenNames);

    toolbarNavigation.goToScenarios();
    assignScenario('Motorika 2', AssignmentType.BACK, undefined, 'Medvídci');

    verifyChildrenQueues('Motorika 2', previousQueueContent, AssignmentType.BACK);
  });

  it('should assign "Řeč - lehčí" correctly to group "Berušky"', async () => {
    childrenPage.navigateTo();
    // Check first 5 children
    const childrenNames = (await childrenPage.getChildrenLastNames('Berušky')).slice(0, 5);
    const previousQueueContent = await getChildrenQueues(childrenNames);

    toolbarNavigation.goToScenarios();
    assignScenario('Řeč - lehčí', AssignmentType.FRONT, undefined, 'Berušky');

    verifyChildrenQueues('Řeč - lehčí', previousQueueContent, AssignmentType.FRONT);
  });

});
